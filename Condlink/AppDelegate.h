//
//  AppDelegate.h
//  Condlink
//
//  Created by macbook on 09/04/15.
//  Copyright (c) 2015 Condlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

