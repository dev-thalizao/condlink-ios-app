 //
//  ViewController.m
//  Condlink
//
//  Created by macbook on 09/04/15.
//  Copyright (c) 2015 Condlink. All rights reserved.
//

#import "MainViewController.h"
#import "Urls.h"
#import "DataManager.h"
#import "SystemManager.h"
#import "NetworkManager.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@interface MainViewController(){
    UIActivityIndicatorView *activityView;
    UIAlertView *alert;
}
@end

@implementation MainViewController

@synthesize webView = _webView;
@synthesize webViewError = _webViewError;
@synthesize backButton = _backButton;
@synthesize forwardButton = _forwardButton;
@synthesize toolbar = _toolbar;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self firstLoad];
    NSLog(@"%@", [[SystemManager sharedInstance] deviceInfo]);
    
//    [[Crashlytics sharedInstance] crash];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    [self baseLayout];
}

- (void)baseLayout
{
    // Get size of screen
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    // Get size of view
    CGRect viewFrame = self.view.frame;
    CGFloat viewWidth = viewFrame.size.width;
    CGFloat viewHeight = viewFrame.size.height;
    NSString *modo;
    
    // Margin top for web view
    CGFloat statusBar;
    
    // Logs
    if(viewHeight > viewWidth){
        modo = @"Portrait";
        statusBar = [UIApplication sharedApplication].statusBarFrame.size.height;
    } else {
        modo = @"Landscape";
        // Simulador funciona se colocar height em vez de width - iphone tem q ser width
        statusBar = [UIApplication sharedApplication].statusBarFrame.size.height;
    }
    
    CGFloat marginTop = statusBar;
    
    // Set in frames
    _webView.frame = CGRectMake(0, marginTop, viewWidth, viewHeight - 44 - marginTop);
    _toolbar.frame = CGRectMake(0, _webView.frame.size.height + marginTop, viewWidth, 44);
    
    NSLog(@"==========================================================================");
    NSLog(@"\n%@\n", modo);
    NSLog(@"Medidas da tela - Altura: %f - Largura: %f\n", screenRect.size.height, screenRect.size.width);
    NSLog(@"Medidas da view - Altura: %f - Largura: %f\n", viewHeight, viewWidth);
    NSLog(@"Medidas da webView - Point: %@ - Size: %@\n", NSStringFromCGPoint(_webView.frame.origin), NSStringFromCGSize(_webView.frame.size));
    NSLog(@"Medidas da toolbar - Point: %@ - Size: %@\n", NSStringFromCGPoint(_toolbar.frame.origin), NSStringFromCGSize(_toolbar.frame.size));
    NSLog(@"==========================================================================");
    
    if(!activityView){
        // Instanciate
        activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        
        // Gear color
        UIColor *gearColor = [UIColor colorWithRed:(0/255.0) green:(122/255.0) blue:(255/255.0) alpha:1.0];
        [activityView setColor:gearColor];
        
        // Add activityView in view and set center
        [self.view addSubview:activityView];
        activityView.frame = CGRectMake(0, 0, 60, 60);
        activityView.center = self.view.center;
    } else {
        // Refresh center view
        activityView.frame = CGRectMake(0, 0, 60, 60);
        activityView.center = self.view.center;
    }
}

- (void) firstLoad {
    // Delegates
    _webView.delegate = self;
    
    // Disable buttons
    [_backButton setEnabled:false];
    [_forwardButton setEnabled:false];
    
    // Load base layout
    [self baseLayout];
    
    // Load the base url
    [self loadUrl:BASE_URL];

}

- (void)loadUrl :(NSString *)fullURL {
    NSURL *url = [NSURL URLWithString:fullURL];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:requestObj];
}

// WebView Delegate
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    NSLog(@"Começou a carregar");
    [activityView startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"Acabou de carregar");
    [activityView stopAnimating];
    
    NSURLRequest *request = [webView request];
    NSString *currentUrl = [[request URL] absoluteString];
    
    @try {
        // Remove http params
        NSRange range = [currentUrl rangeOfString:@"?"];
        if(!(range.location == NSNotFound)){
            currentUrl = [currentUrl substringToIndex:range.location];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Exception on remove query params: %@", exception);
    }
    
    // Custom logic
    if([currentUrl isEqualToString:LOGIN_URL])
    {
        [[SystemManager sharedInstance] unregisterForRemoteNotifications];
    }
    else if([currentUrl isEqualToString:SIGNED_URL])
    {
        NSString *userID = [webView stringByEvaluatingJavaScriptFromString:@"getUserId()"];
        if(userID && ![userID isEqualToString:@""]) {
            [[SystemManager sharedInstance] registerRemoteNotificationsWithID:userID];
        }
    }
    [self requestDidLoad];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"Erro - %@", error.description);
    [activityView stopAnimating];
    [self showAlertWithTitle:@"Erro" Message:[self getErrorMessageWithError:error] Tag:0];
    [self requestDidLoad];
}

// After execute every request
- (void) requestDidLoad {
    // Hablita o botao de voltar caso de pra voltar
    if(_webView.canGoBack){
        [_backButton setEnabled:true];
    } else {
        [_backButton setEnabled:false];
    }
    
    // Habilita o botao de avançar caso de pra avançar
    if(_webView.canGoForward){
        [_forwardButton setEnabled:true];
    } else {
        [_forwardButton setEnabled:false];
    }
    
    // Habilita o botao de reload
    [_refreshButton setEnabled:YES];
}

// WebView navigation
- (IBAction)onClickBackButton:(id)sender {
    if(_webView.canGoBack){
        [_webView goBack];
    }
}

- (IBAction)onClickForwardButton:(id)sender {
    if(_webView.canGoForward){
        [_webView goForward];
    }
}

- (IBAction)onClickRefreshButton:(id)sender {
    NSURLRequest *request = [_webView request];
    NSString *currentUrl = [[request URL] absoluteString];
    
    if([currentUrl rangeOfString:BASE_URL].location == NSNotFound){
        currentUrl = LOGIN_URL;
    }
    
    [self loadUrl:currentUrl];
    [_webView setUserInteractionEnabled: YES];
}

// Alert delegate
- (void)showAlertWithTitle :(NSString *)title Message:(NSString *)message Tag:(int)tag
{
    _alertView = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    _alertView.tag = tag;
    [_alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (alertView.tag) {
        case 0: {
            [_webView setUserInteractionEnabled: NO];
        }
        break;
            
        default: break;
    }
}

- (NSString *)getErrorMessageWithError :(NSError*)error
{
    NSString *message = @"";
    //  error.code = -1009 sem internet -1004 - nao pode conectar ao servidor
    switch (error.code) {
        case -1004:
            message = @"A aplicação não pode se conectar com o servidor! Por favor, verifique sua conexão com a internet ou entre em contato com o administrador.";
        break;
            
        default:
            message = @"Erro! Contate o administrador do sistema!";
        break;
    }
    return message;
}
@end
