//
//  Urls.h
//  Condlink
//
//  Created by macbook on 04/05/15.
//  Copyright (c) 2015 Condlink. All rights reserved.
//

#ifndef Condlink_Urls_h
#define Condlink_Urls_h

#define BASE_URL @"http://qa.condlink.com.br:8080/mobile"
//#define BASE_URL @"http://10.1.2.150:8080/mobile"
#define LOGIN_URL  BASE_URL@"/login/auth"
#define SIGNED_URL BASE_URL@"/"
#define NEW_DEVICE_URL BASE_URL@"/dispositivo/vincularDispositivo.json"
#define CHANGE_PUSH_STATUS_URL BASE_URL@"/dispositivo/trocarStatusNotificacao.json"

#endif
