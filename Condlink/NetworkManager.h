//
//  NetworkManager.h
//  Condlink
//
//  Created by macbook on 06/05/15.
//  Copyright (c) 2015 Condlink. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkManager : NSObject
+ (id)sharedInstance;
- (void)saveDevice :(NSDictionary *)params;
- (void)changePushStatus :(NSMutableDictionary *)params statusPush:(BOOL)pushStatus;
@end
