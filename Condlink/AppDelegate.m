//
//  AppDelegate.m
//  Condlink
//
//  Created by macbook on 09/04/15.
//  Copyright (c) 2015 Condlink. All rights reserved.
//

#import "AppDelegate.h"
#import "DataManager.h"
#import "SystemManager.h"
#import "Constants.h"
#import "JCNotificationCenter.h"
#import "JCNotificationBannerPresenterIOS7Style.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>


@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    //Save uniqueId on cache
    [[DataManager sharedInstance] saveDataToUserDefaultKey:kUNIQUE_ID Object:[[SystemManager sharedInstance] deviceIdentifierForVendor]];
    [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    [Fabric with:@[CrashlyticsKit]];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

// methods called after registerNotification
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSLog(@"==========================================================================\nRegister Success");
    NSLog(@"Did Register for Remote Notifications with Device Token (%@)", deviceToken);
    [[DataManager sharedInstance] saveDataToUserDefaultKey:kTOKEN_PUSH Object: [NSString stringWithFormat:@"%@", deviceToken]];
    [[SystemManager sharedInstance] prepareToSaveDevice];
    NSLog(@"==========================================================================");
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"==========================================================================\nRegister Fail");
    NSLog(@"Did Fail to Register for Remote Notifications");
    NSLog(@"%@, %@", error, error.localizedDescription);
    NSLog(@"==========================================================================");
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    //Criar logica para abrir url especifica
    NSLog(@"==========================================================================\nPush Receive");
    NSLog(@"User Info (%@)", userInfo);
    
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive) {
        
        NSDictionary *aps = [userInfo objectForKey:@"aps"];
        NSDictionary *alert = [aps objectForKey:@"alert"];
        NSString *title = [alert objectForKey:@"title"];
        NSString *message = [alert objectForKey:@"body"];
        NSString *action = [alert objectForKey:@"action"];

        JCNotificationCenter *jcnc = [[JCNotificationCenter alloc] init];
        jcnc.presenter = [[JCNotificationBannerPresenterIOS7Style alloc] init];
        [jcnc enqueueNotificationWithTitle:title message:message tapHandler:^{
             NSLog(@"Received tap on notification banner! - Action %@", action);
         }];
    }
    NSLog(@"==========================================================================");
}

//-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
//{
//    [UIApplication sharedApplication].applicationIconBadgeNumber = 10;
//    completionHandler(UIBackgroundFetchResultNewData);
//}

@end
