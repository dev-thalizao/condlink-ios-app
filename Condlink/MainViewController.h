//
//  ViewController.h
//  Condlink
//
//  Created by macbook on 09/04/15.
//  Copyright (c) 2015 Condlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController<UIWebViewDelegate, UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic) BOOL webViewError;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *forwardButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *refreshButton;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property (strong, nonatomic) UIAlertView *alertView;

- (IBAction)onClickBackButton:(id)sender;
- (IBAction)onClickForwardButton:(id)sender;
- (IBAction)onClickRefreshButton:(id)sender;
@end

