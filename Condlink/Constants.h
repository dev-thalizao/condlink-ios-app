//
//  Constants.h
//  Condlink
//
//  Created by macbook on 08/05/15.
//  Copyright (c) 2015 Condlink. All rights reserved.
//

#ifndef Condlink_Constants_h
#define Condlink_Constants_h

// User data
#define kUSER_ID @"usuarioId"
#define kDEVICE_ID @"id"
#define kTOKEN_PUSH @"token"
#define kNOTIFICATION_ENABLE @"notificacaoAtiva"
#define kUNIQUE_ID @"idDispositivo"

// Device data
#define kMODEL @"modelo"
#define kLOCALIZED_MODEL @"localizedModel"
#define kDEVICE_NAME @"nome"
#define kSYSTEM_NAME @"sistema"
#define kSYSTEM_VERSION @"versaoSistema"
#define kIOS @"IOS"

// Log tag
#define kDATA_LOG @"\nDATA_MANAGER"
#define kNETWORK_LOG @"\nNETWORK_MANAGER"
#define kSYSTEM_LOG @"\nSYSTEM_MANAGER"
#define kVIEW_LOG @"\nVIEW_LOG"

// Error
#define kERROR @"erro"

#endif