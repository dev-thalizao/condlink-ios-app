//
//  DataManager.m
//  Condlink
//
//  Created by macbook on 05/05/15.
//  Copyright (c) 2015 Condlink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataManager.h"
#import "Constants.h"

@implementation DataManager : NSObject

+ (id)sharedInstance {
    static DataManager *uniqueInstance = nil;
    @synchronized(self) {
        if (uniqueInstance == nil)
            uniqueInstance = [[self alloc] init];
    }
    return uniqueInstance;
}

- (void)saveDataToUserDefaultKey:(NSString *)key Object:(id)obj
{
    NSUserDefaults *user = [self getUser];
    [user setObject:obj forKey:key];
    if([user synchronize])
        NSLog(@"%@:Data save - Success", kDATA_LOG);
}

// Save full Dictionary if oneByOne is true, if not, save all the values of the dictionary
- (void)saveMultipleDataToUserDefault:(NSMutableDictionary *)dictionary :(BOOL)oneByOne :(NSString *)key
{
    if(oneByOne){
        // Chamar o metodo direto aqui, para chamar o synchronize uma vez só
        NSUserDefaults *user = [self getUser];
        for(NSString *k in dictionary){
            [user setObject:[dictionary objectForKey:k] forKey:k];
        }
        if([user synchronize])
            NSLog(@"%@:Dictionary - Save all values", kDATA_LOG);

    } else {
        [self saveDataToUserDefaultKey:key Object:dictionary];
    }
}

- (id)getDataFromUserDefaultFor:(NSString *)key
{
    return [[self getUser] objectForKey:key];
}

- (void)removeObjForKey:(NSString *)key
{
    NSUserDefaults *user = [self getUser];
    [user setObject:@"" forKey:key];
    [user removeObjectForKey:key];
    if([user synchronize])
        NSLog(@"%@:Remove - Success", kDATA_LOG);
}

- (NSUserDefaults *) getUser
{
    return [NSUserDefaults standardUserDefaults];
}
@end
