//
//  SystemManager.h
//  Condlink
//
//  Created by macbook on 05/05/15.
//  Copyright (c) 2015 Condlink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SystemManager : NSObject
+ (id)sharedInstance;
- (void)registerRemoteNotificationsWithID:(NSString *)userID;
- (void)unregisterForRemoteNotifications;
- (void)prepareToSaveDevice;
- (NSMutableDictionary *)deviceInfo;
- (NSString *)deviceIdentifierForVendor;
@end
