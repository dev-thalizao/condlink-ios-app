//
//  SystemManager.m
//  Condlink
//
//  Created by macbook on 05/05/15.
//  Copyright (c) 2015 Condlink. All rights reserved.
//

#import "SystemManager.h"
#import "DataManager.h"
#import "NetworkManager.h"
#import "Constants.h"

@implementation SystemManager
+ (id)sharedInstance {
    static SystemManager *uniqueInstance = nil;
    @synchronized(self) {
        if (uniqueInstance == nil)
            uniqueInstance = [[self alloc] init];
    }
    return uniqueInstance;
}

- (void)registerRemoteNotificationsWithID:(NSString *)userID
{
    // Salve user on cache
    [[DataManager sharedInstance] saveDataToUserDefaultKey:kUSER_ID Object:userID];
    NSString *token = [[DataManager sharedInstance] getDataFromUserDefaultFor:kTOKEN_PUSH];
    if(!token){
        // Register for remote notifications
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
            UIUserNotificationSettings *settings =
            [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert |
             UIUserNotificationTypeBadge |
             UIUserNotificationTypeSound
                                              categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        } else {
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
             UIRemoteNotificationTypeAlert |
             UIRemoteNotificationTypeBadge |
             UIRemoteNotificationTypeSound];
        }
    }
}

- (void)unregisterForRemoteNotifications
{
    DataManager *manager = [DataManager sharedInstance];
    
    NSNumber *pushStatus = [manager getDataFromUserDefaultFor:kNOTIFICATION_ENABLE];
    //concertar oush
    if([pushStatus boolValue]){
        NSString *token = [manager getDataFromUserDefaultFor:kTOKEN_PUSH];
        NSString *userID = [manager getDataFromUserDefaultFor:kUSER_ID];
        NSString *deviceID = [manager getDataFromUserDefaultFor:kDEVICE_ID];
        NSString *uniqueID = [manager getDataFromUserDefaultFor:kUNIQUE_ID];
        
        if(token){
            [[UIApplication sharedApplication] unregisterForRemoteNotifications];
            [manager removeObjForKey:kTOKEN_PUSH];
        }
        
        if(deviceID && userID){
            NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
            [params setObject:deviceID forKey:kDEVICE_ID];
            [params setObject:uniqueID forKey:kUNIQUE_ID];
            [params setObject:userID forKey:kUSER_ID];
            [params setObject:@"" forKey:kTOKEN_PUSH];
            
            NetworkManager *networkManager = [NetworkManager sharedInstance];
            [networkManager changePushStatus:params statusPush:FALSE];
        }
    } else {
        NSLog(@"%@: Notificacao ja esta desativada!", kSYSTEM_LOG);
    }
}

- (void)prepareToSaveDevice
{
    DataManager *manager = [DataManager sharedInstance];
    NSString *token = [manager getDataFromUserDefaultFor:kTOKEN_PUSH];
    NSString *userID = [manager getDataFromUserDefaultFor:kUSER_ID];
    NSString *deviceID = [manager getDataFromUserDefaultFor:kDEVICE_ID];
    NSMutableDictionary *info = [self deviceInfo];
    
    if(deviceID){
        [info setObject:deviceID forKey:kDEVICE_ID];
    }
    
    // Token e usuario sempre vão existir
    [info setObject:userID forKey:kUSER_ID];
    [info setObject:token forKey:kTOKEN_PUSH];
    
    NSLog(@"%@: %@", kSYSTEM_LOG, info);
    
    // Chama NetworkManager pra salvar o device
    NetworkManager *networkManager = [NetworkManager sharedInstance];
    [networkManager saveDevice:info];
}

// Get device info
- (NSMutableDictionary *)deviceInfo
{
    NSMutableDictionary *info =[[NSMutableDictionary alloc]init];
    UIDevice *device = [UIDevice currentDevice];
    
    [info setValue:[device localizedModel] forKey:kLOCALIZED_MODEL];
    [info setValue:[device name] forKey:kDEVICE_NAME];
    [info setValue:[self deviceIdentifierForVendor] forKey:kUNIQUE_ID];
    [info setValue:kIOS forKey:kSYSTEM_NAME];
    [info setValue:[device systemVersion] forKey:kSYSTEM_VERSION];
    [info setValue:[device model] forKey:kMODEL];
    return info;
}

- (NSString *)deviceIdentifierForVendor
{
    NSString *difv = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    return difv;
}
@end
