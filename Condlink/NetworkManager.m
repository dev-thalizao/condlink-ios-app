//
//  NetworkManager.m
//  Condlink
//
//  Created by macbook on 06/05/15.
//  Copyright (c) 2015 Condlink. All rights reserved.
//

#import "NetworkManager.h"
#import "AFNetworking.h"
#import "DataManager.h"
#import "Urls.h"
#import "Constants.h"
#import "JCNotificationCenter.h"
#import "JCNotificationBannerPresenterIOS7Style.h"

@implementation NetworkManager

+ (id)sharedInstance {
    static NetworkManager *uniqueInstance = nil;
    @synchronized(self) {
        if (uniqueInstance == nil)
            uniqueInstance = [[self alloc] init];
    }
    return uniqueInstance;
}

- (void)get:(NSString *)url params:(NSDictionary *)params block:(void(^)(id response)) completion
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@:POST_JSON: %@", kNETWORK_LOG, responseObject);
        completion(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@:Error: %@", kNETWORK_LOG, error);
    }];
}

- (void)post:(NSString *)url params:(NSDictionary *)params block:(void(^)(id response)) completion
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSDictionary *parameters = params;
    
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@:POST_JSON: %@", kNETWORK_LOG, responseObject);
        completion(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@:Error: %@", kNETWORK_LOG, error);
    }];
}

- (void)saveDevice:(NSMutableDictionary *)params
{
    NSString *deviceID = [params objectForKey:kDEVICE_ID];
    // Se existir deviceID, quer dizer que o aparelho ja foi cadastrado na web (então só vai mudar o push status), se não cria um cadastro novo
    if(deviceID){
        [self changePushStatus:params statusPush:YES];
    } else {
        [self saveNewDevice:params];
    }
}

- (void)saveNewDevice :(NSDictionary *)params
{
    [self post:NEW_DEVICE_URL params:params block:^(id response) {
        NSDictionary *json_response;
        
        @try{
            json_response = [[NSDictionary alloc] initWithDictionary:response];
        } @catch (NSException *e) {
            return;
        }
        
        NSString *errorMessage = [json_response objectForKey:kERROR];
        if(!errorMessage){
            // Pega o device id
            NSString *deviceID =  [NSString stringWithFormat:@"%@", [json_response objectForKey:kDEVICE_ID]];
            NSNumber *pushStatus = [json_response objectForKey:kNOTIFICATION_ENABLE];
            
            // Salva os dados no cache
            DataManager *manager = [DataManager sharedInstance];
            [manager saveDataToUserDefaultKey:kDEVICE_ID Object:deviceID];
            [manager saveDataToUserDefaultKey:kNOTIFICATION_ENABLE Object:pushStatus];
            // Como ja vai vir um push se o status for true, não precisa de msg de sucesso
        } else {
            [self createAlertMessageTitle:@"Erro" Message:errorMessage];
        }
    }];
}

- (void)changePushStatus :(NSMutableDictionary *)params statusPush:(BOOL)pushStatus
{
    [params setObject:[NSNumber numberWithBool:pushStatus] forKey:kNOTIFICATION_ENABLE];
    [self post:CHANGE_PUSH_STATUS_URL params:params block:^(id response) {
        NSDictionary *json_response;
        
        @try{
            json_response = [[NSDictionary alloc] initWithDictionary:response];
        } @catch (NSException *e) {
            return;
        }
        
        NSString *errorMessage = [json_response objectForKey:kERROR];
        if(!errorMessage){
            // Pega device id e status da notificacao
            NSString *deviceID = [json_response objectForKey:kDEVICE_ID];
            NSNumber *pushStatus = [json_response objectForKey:kNOTIFICATION_ENABLE];
            
            // Salva os dados no cache
            DataManager *manager = [DataManager sharedInstance];
            [manager saveDataToUserDefaultKey:kDEVICE_ID Object:deviceID];
            [manager saveDataToUserDefaultKey:kNOTIFICATION_ENABLE Object:pushStatus];
            // Como ja vai vir um push se o status for true, não precisa de msg de sucesso
            if(![pushStatus boolValue]){
                [self createAlertMessageTitle:@"Notificações Desativadas" Message:@"Logue na aplicação para reativar!"];
            }
        } else {
            [self createAlertMessageTitle:@"Erro" Message:errorMessage];
        }
    }];
}

-(void)createAlertMessageTitle :(NSString *)title Message :(NSString *)message
{
    NSLog(@"%@: - %@ - %@", kNETWORK_LOG, title, message);
    JCNotificationCenter *jcnc = [[JCNotificationCenter alloc] init];
    jcnc.presenter = [[JCNotificationBannerPresenterIOS7Style alloc] init];
    [jcnc enqueueNotificationWithTitle:title message:message tapHandler:nil];
}

@end
