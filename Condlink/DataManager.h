//
//  DataManager.h
//  Condlink
//
//  Created by macbook on 05/05/15.
//  Copyright (c) 2015 Condlink. All rights reserved.
//

@interface DataManager : NSObject
+ (id)sharedInstance;
- (void)saveDataToUserDefaultKey:(NSString *)key Object:(id)obj;
- (id)getDataFromUserDefaultFor :(NSString *)key;
- (void)removeObjForKey:(NSString *)key;
@end