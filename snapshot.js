#import "SnapshotHelper.js"

// Get core elements

// Local target is the running simulator
var target = UIATarget.localTarget();
// Get the frontmost app running in the target
var app = target.frontMostApp();
// Grab the main window of the application
var window = app.mainWindow();
// Grag log
var log = UIALogger;

// Pause for 6 seconds to wait page load
target.delay(15);
// Wait indicator finished
wait_for_loading_indicator_to_be_finished();

log.logMessage("Tirando print da tela de login");
captureLocalizedScreenshot("LOGIN");

// Get webView
var webView = window.scrollViews()[0].webViews()[0];

// Log webView
// webView.logElementTree();

// Login click and set value
webView.textFields()[0].tap();
target.delay(3);
webView.textFields()[0].setValue("morador.atrium");

// Delay to prevent a error
target.delay(3);

// Password click and set value
webView.secureTextFields()[0].tap();
target.delay(3);
webView.secureTextFields()[0].setValue("123");

// Delay to prevent a error
target.delay(3);

// Tap on login button
webView.buttons()[0].tap();

target.delay(20);

// on index page
log.logMessage("Tirando print da index");
captureLocalizedScreenshot("INDEX");

// Tap on hamburger button
webView.buttons()[0].tap();
target.delay(10);

// on menu on index page
log.logMessage("Tirando print do menu");
captureLocalizedScreenshot("MENU");

target.delay(3);

// TAP 'meus dados' link
webView.links()[1].tap();
target.delay(20);

// 'meus dados' page
log.logMessage("Tirando print do meus dados");
captureLocalizedScreenshot("MEUSDADOS");

// Tap on hamburger button
target.delay(3);
webView.buttons()[0].tap();
target.delay(3);

// TAP 'grupos' link
webView.links()[3].tap();

target.delay(20);

// 'grupos' page
log.logMessage("Tirando print do grupo");
captureLocalizedScreenshot("GRUPO");

//webView.logElementTree();

